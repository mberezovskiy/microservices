﻿namespace EmailService.DTO.Models
{
    public class RecipientModelDto
    {
        /// <summary>
        /// Recipient's email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Recipient's name.
        /// </summary>
        public string Name { get; set; }
    }
}
