﻿using System;
using System.Collections.Generic;

namespace EmailService.DTO.Models
{
    /// <summary>
    /// Model, contains all nedded data for sending emails.
    /// </summary>
    public class EmailDataDto
    {
        /// <summary>
        /// Mail adresses where need to send message.
        /// </summary>
        public IEnumerable<RecipientModelDto> Tos { get; set; }
        /// <summary>
        /// Mail adresses, who must receive an email, but not primary recipient.
        /// </summary>
        public IEnumerable<RecipientModelDto> Ccs { get; set; }
        /// <summary>
        /// Mail adresses, who must receive an email, but other recipients should not see them.
        /// </summary>
        public IEnumerable<RecipientModelDto> Bccs { get; set; }
        /// <summary>
        /// Email attachments.
        /// </summary>
        public IEnumerable<AttachmentDto> Attachments { get; set; }

        /// <summary>
        /// Date/Time when need to save email.
        /// </summary>
        public DateTime? SendAt { get; set; }
        /// <summary>
        /// Person, from who send email.
        /// </summary>
        public RecipientModelDto From { get; set; }
        /// <summary>
        /// Mail subject.
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// Text content. Will be placed in the mail, if mail doesn't have HTML Content.
        /// </summary>
        public string TextContent { get; set; }
        /// <summary>
        /// HTML Template with text. Will be placed in the mail.
        /// </summary>
        public string HtmlContent { get; set; }
    }
}
