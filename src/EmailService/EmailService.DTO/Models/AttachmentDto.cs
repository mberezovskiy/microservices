﻿namespace EmailService.DTO.Models
{
    /// <summary>
    /// Represents attached file.
    /// </summary>
    public class AttachmentDto
    {
        /// <summary>
        /// File name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// File data, encoded as base64string.
        /// </summary>
        public string Data { get; set; }
    }
}
