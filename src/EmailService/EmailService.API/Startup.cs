﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EmailService.API.Configuration;
using EmailService.Core.EmailSevices;
using EmailService.Core.Facades;
using EmailService.Core.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace EmailService.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //services.AddSingleton<IConfiguration>(Configuration);
            services.AddAutoMapper(AutoMapperConfiguration.Configure);

            // Facades
            services.AddScoped<IEmailFacade, EmailFacade>();
            // Services
            services.AddScoped<IEmailService>(s => new SendGridMailService(Configuration["SendGrid:ApiKey"]));


            // Register the Swagger generator
            services.AddSwaggerGen(SwaggerConfiguration.ConfigureSwaggerGen);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Enable Swagger middleware.
            app.UseSwagger(SwaggerConfiguration.ConfigureSwagger);
            app.UseSwaggerUI(SwaggerConfiguration.ConfigureSwaggerUI);

            app.UseMvc();
        }
    }
}
