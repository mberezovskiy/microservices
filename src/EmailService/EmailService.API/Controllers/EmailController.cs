﻿using System;
using System.Threading.Tasks;
using EmailService.Core.Interfaces;
using EmailService.DTO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.API.Controllers
{
    [Route("api")]
    public class EmailController : Controller
    {
        private readonly IEmailFacade _emailFacade;

        public EmailController(IEmailFacade emailFacade)
        {
            _emailFacade = emailFacade;
        }

        [HttpPost("sendmail")]
        public async Task<IActionResult> SendMail(EmailDataDto emailDataDTO)
        {
            try
            {
                await _emailFacade.SendMailAsync(emailDataDTO);

                return Accepted();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("test")]
        public IActionResult TestSending()
        {
            return View();
        }
    }
}