﻿using AutoMapper;
using EmailService.API.Configuration.AutoMapperConventors;
using EmailService.Core.Models;
using EmailService.DTO.Models;

namespace EmailService.API.Configuration
{
    public static class AutoMapperConfiguration
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RecipientModelDto, RecipientModel>();
            cfg.CreateMap<EmailDataDto, EmailModel>()
                .ForMember(dest => dest.Attachments, opt => opt.MapFrom(src => src.Attachments));
            // todo test convert
             cfg.CreateMap<AttachmentDto, AttachmentModel>();
            cfg.CreateMap<string, byte[]>().ConvertUsing(new StringByteArrayConventor());
        }
    }
}
