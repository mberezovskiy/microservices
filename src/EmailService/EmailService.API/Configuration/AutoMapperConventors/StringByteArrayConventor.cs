﻿using AutoMapper;

namespace EmailService.API.Configuration.AutoMapperConventors
{
    public class StringByteArrayConventor : ITypeConverter<string, byte[]>
    {
        public byte[] Convert(string source, byte[] destination, ResolutionContext context)
        {
            return System.Convert.FromBase64String(source);
        }
    }
}
