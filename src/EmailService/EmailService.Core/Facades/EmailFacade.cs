﻿using AutoMapper;
using EmailService.Core.Interfaces;
using EmailService.Core.Models;
using EmailService.DTO.Models;
using System.Threading.Tasks;

namespace EmailService.Core.Facades
{
    /// <summary>
    /// Default implementation IEmailFacade.
    /// </summary>
    public class EmailFacade : IEmailFacade
    {
        private readonly IEmailService _mailService;
        private readonly IMapper _mapper;

        /// <summary>
        /// EmailFacade constructor.
        /// </summary>
        /// <param name="mailService">Realization of the IEmailService, that is used to send messages.</param>
        /// <param name="mapper">Realization of the IMapper, that is used to map data.</param>
        public EmailFacade(IEmailService mailService, IMapper mapper)
        {
            _mailService = mailService;
            _mapper = mapper;
        }

        public async Task SendMailAsync(EmailDataDto emailDataDTO)
        {
           await _mailService.SendMailAsync(_mapper.Map<EmailModel>(emailDataDTO));
        }
    }
}
