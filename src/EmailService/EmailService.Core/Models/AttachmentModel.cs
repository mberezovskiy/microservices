﻿namespace EmailService.Core.Models
{
    /// <summary>
    /// Represents attached file.
    /// </summary>
    public class AttachmentModel
    {
        /// <summary>
        /// File name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// File data.
        /// </summary>
        public byte[] Data { get; set; }
    }
}
