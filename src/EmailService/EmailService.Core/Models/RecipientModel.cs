﻿namespace EmailService.Core.Models
{
    /// <summary>
    /// Represents email's recipient.
    /// </summary>
    public class RecipientModel
    {
        /// <summary>
        /// Recipient's email.
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Recipient's name.
        /// </summary>
        public string Name { get; set; }
    }
}
