﻿using EmailService.Core.Interfaces;
using EmailService.Core.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EmailService.Core.EmailSevices
{
    /// <summary>
    /// IEmailService implementation using SendGrid.
    /// </summary>
    public class SendGridMailService : IEmailService
    {
        private readonly string _apiKey;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="apiKey">SendGrid api key.</param>
        public SendGridMailService(string apiKey)
        {
            _apiKey = apiKey;
        }

        public async Task SendMailAsync(EmailModel emailModel)
        {
            var client = new SendGridClient(_apiKey);

            var msg = new SendGridMessage()
            {
                From = new EmailAddress(emailModel.From.Email, emailModel.From.Name),
                Subject = emailModel.Subject
            };
            // adds TO's
            if (emailModel.Tos != null && emailModel.Tos.Any())
            {
                foreach (var to in emailModel.Tos)
                {
                    msg.AddTo(new EmailAddress(to.Email, to.Name));
                }
            }
            // adds CC's
            if (emailModel.Ccs != null && emailModel.Ccs.Any())
            {
                foreach (var cc in emailModel.Ccs)
                {
                    msg.AddCc(new EmailAddress(cc.Email, cc.Name));
                }
            }
            // adds BCC's
            if (emailModel.Bccs != null && emailModel.Bccs.Any())
            {
                foreach (var bcc in emailModel.Bccs)
                {
                    msg.AddBcc(new EmailAddress(bcc.Email, bcc.Name));
                }
            }
            // adds attachments
            if (emailModel.Attachments != null && emailModel.Attachments.Any())
            {
                foreach (var file in emailModel.Attachments)
                {
                    if (file.Data.Length > 0)
                    {
                        msg.AddAttachment(file.Name, Convert.ToBase64String(file.Data));
                    }
                }
            }
            
            // add's content
            if (!string.IsNullOrEmpty(emailModel.HtmlContent))
            {
                msg.HtmlContent = emailModel.HtmlContent;
            } else
            {
                msg.PlainTextContent = emailModel.TextContent;
            }


            // send at. Does't work now, strange behavior of the SendGrid.
            /*var dateTimeOffset = new DateTimeOffset(emailModel.SendAt);
            var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();

            msg.SendAt = unixDateTime;*/

            var response = await client.SendEmailAsync(msg);
        }
    }
}
