﻿using EmailService.Core.Models;
using System.Threading.Tasks;

namespace EmailService.Core.Interfaces
{
    /// <summary>
    /// Basic interface used to sends emails.
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Sends mail with specify data.
        /// </summary>
        Task SendMailAsync(EmailModel emailModel);
    }
}
