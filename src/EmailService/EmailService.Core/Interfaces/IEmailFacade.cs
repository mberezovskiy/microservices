﻿using EmailService.Core.Models;
using EmailService.DTO.Models;
using System.Threading.Tasks;

namespace EmailService.Core.Interfaces
{
    /// <summary>
    /// Interface for all email facades.
    /// </summary>
    public interface IEmailFacade
    {
        /// <summary>
        /// Sends mail to specify adress.
        /// </summary>
        /// <returns></returns>
        Task SendMailAsync(EmailDataDto emailDataDTO);
    }
}
